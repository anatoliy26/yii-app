<?php
    namespace frontend\images;
    
    use frontend\images\ImageWidgetAsset;
    use yii\base\Widget;
    use yii\helpers\Html;

class ImageWidget extends Widget
    {
        public $model;
        
        public function init()
        {
            parent::init();
        }

        public function run()
        {
            ImageWidgetAsset::register($this->getView());
            
            return $this->render('_image',['image' => $this->model]);
        }
    }
?>