<?php
namespace frontend\images;
use yii\web\AssetBundle;

class ImageWidgetAsset extends  AssetBundle
{
    public $js = [
        'js/imageWidget.js'
    ];
    public $css = [
        'css/imageWidget.css',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
    
    public function init()
    {
        $this->sourcePath = __DIR__ . '/assets';
        parent::init();
    }
}

?>