<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property int $Rec_ID
 * @property string $ImageName
 * @property string $ImageFilePath
 * @property string $ImageDescription
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ImageName', 'ImageFilePath', 'ImageDescription'], 'required'],
            [['ImageName', 'ImageFilePath', 'ImageDescription'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Rec_ID' => 'Rec ID',
            'ImageName' => 'Image Name',
            'ImageFilePath' => 'Image File Path',
            'ImageDescription' => 'Image Description',
        ];
    }
}
